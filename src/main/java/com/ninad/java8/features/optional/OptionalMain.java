package com.ninad.java8.features.optional;

import java.util.Optional;

public class OptionalMain {

    public static void main(String[] args) {
        String name = "Ninad";
        Optional<String> opt = Optional.of(name);
        System.out.println(opt.orElse("Default Value"));

        String name1 = null;
        Optional<String> opt1 = Optional.ofNullable(name1);
        System.out.println(opt1.orElse("Default Value"));
    }
}