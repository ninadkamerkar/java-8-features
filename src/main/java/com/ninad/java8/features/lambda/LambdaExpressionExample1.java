package com.ninad.java8.features.lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class LambdaExpressionExample1 {

	public static void main(String[] args) {
		System.out.println("------------------- Normal Implementation -------------------");
		Foo normalFoo = new Foo() {
			public void method() {
				System.out.println("Hello Normal Foo Implementation");
			}
		};
		normalFoo.method();

		System.out.println("------------------- Lamda Expression -------------------");
		//lambda expression without parameter and single statement 
		Foo lambdaFoo = () -> System.out.println("Lamda Expression = No param & single body statement without braces");
		lambdaFoo.method();
		
		//lambda expression without parameter and single statement 
		Foo lambdaFoo1 = () -> { System.out.println("Lamda Expression = No param & single body statement with braces"); };
		lambdaFoo1.method();

		Boo lambdaBoo = (message) -> System.out.println("Lamda Expression = With single param = "+message);
		lambdaBoo.method("Ninad");
		
		//Avoid Parentheses Around a Single Parameter
		Boo lambdaBoo2 = message -> System.out.println("Lamda Expression = Avoid Parentheses Around a Single Parameter = "+message);
		lambdaBoo2.method("Ninad");		
		
		
		//A compiler in most cases is able to resolve the type of lambda parameters with the help of type inference. 
		//Therefore, adding a type to the parameters is optional and can be omitted.
		Boo lambdaBoo1 = (String message) -> System.out.println("Lamda Expression = Specifying Parameter Types (Optional) = "+message);
		lambdaBoo1.method("Ninad");		
		
		Too lambdaToo = (name) -> { return "Hello "+name;  };
		String msg = lambdaToo.method("Ninad");	
		System.out.println("Lamda Expression = Return Statement and Braces. o/p = "+msg);
		
		Too lambdaToo1 = (name) -> "Hello "+name;
		String msg1 = lambdaToo1.method("Ninad");	
		System.out.println("Lamda Expression = Avoid Return Statement and Braces. o/p = "+msg1);	
		
		//Functional interfaces can be extended by other functional interfaces 
		//if their abstract methods have the same signature.
		ExtendedFuncInterface extendedFuncInterface = (message) -> System.out.println("Lamda Expression = Extended Func Interface = "+message);
		extendedFuncInterface.method("Ninad");
		
		//Passing lambda function to normal method
		Foo foo = () -> System.out.println("Passing lambda function to normal method");
		LambdaExpressionExample1.acceptLamdaExpression(foo);
		
		System.out.println("------------------- In-Built apis taking lamda expression -------------------");
		List<String> lstNames = new ArrayList<String>();
		lstNames.add("Ninad");
		lstNames.add("Shruti");
		lstNames.add("Rutu");
		
		System.out.println("list.forEach");
		lstNames.forEach(names -> System.out.println("\t"+names));
		
		System.out.println("------------------- In-Built functional interface (Runnable, Comparator) -------------------");
		Runnable runnable = () -> System.out.println("Thread Started = "+Thread.currentThread().getName());
		Thread thread = new Thread(runnable);
		thread.start();
		
		Comparator<String> comp = (val1, val2) -> val1.compareTo(val2);
		
		 Collections.sort(lstNames, comp);
		 System.out.println("Sorted Collection = "+lstNames);
		 
		 System.out.println("------------------- Consumer<T> -------------------");
        // This consumer calls a void method with the value.
        Consumer<Integer> consumer = x -> display(x);

        // Use the consumer with three numbers.
        consumer.accept(2);

        System.out.println("------------------- Function<T,T> -------------------");
        // Create a Function from a lambda expression.
        // It returns the argument multiplied by two.
        Function<Integer, Integer> func = x -> x * 2;

        // Apply the function to an argument of 10.
        int result = func.apply(10);
        System.out.println(result);		 
        
        System.out.println("------------------- Predicate<T> -------------------");
        // Create ArrayList and add four String elements.
        ArrayList<String> list = new ArrayList<>();
        list.add("cat");
        list.add("dog");
        list.add("cheetah");
        list.add("deer");
        
        // Remove elements that start with c.
        Predicate<String> predicate = element -> element.startsWith("c");
        list.removeIf(predicate);
        System.out.println(list.toString());        
        
        System.out.println("------------------- UnaryOperator<T> -------------------");
        // This returns one value of the same type as its one parameter.
        // ... It means the same as the Function below.
        UnaryOperator<Integer> operator = v -> v * 100;

        // This is a generalized form of UnaryOperator.
        Function<Integer, Integer> function = v -> v * 100;

        System.out.println(operator.apply(5));
        System.out.println(function.apply(6));	
        
        System.out.println("------------------- BiConsumer<T,T> -------------------");
        HashMap<String, String> hash = new HashMap<>();
        hash.put("cat", "orange");
        hash.put("dog", "black");
        hash.put("snake", "green");
        
        
        // Use lambda expression that matches BiConsumer to display HashMap.
        BiConsumer<String, String> biConsumer = (key, value) -> System.out.println("Key = "+key + ", Value = "+ value);
        hash.forEach(biConsumer);       
		
	}
	
	static void acceptLamdaExpression(Foo foo) {
		foo.method();
	}
	
    static void display(int value) {

        switch (value) {
        case 1:
            System.out.println("There is 1 value");
            return;
        default:
            System.out.println("There are " + Integer.toString(value)
                    + " values");
            return;
        }
    }	
	
	//Optional but need to provide as per better standards as it gives warning 
	//if you add another abstract method to functional interface
	@FunctionalInterface
	interface Foo {
		public void method();
	}
	
	@FunctionalInterface
	interface Boo {
		public void method(String msg);
	}	
	
	@FunctionalInterface
	interface Too {
		public String method(String name);
	}
	
	@FunctionalInterface
	interface ExtendedFuncInterface extends Boo {
		public void method(String name);
	}	
	
}
