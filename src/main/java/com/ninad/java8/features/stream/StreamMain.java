package com.ninad.java8.features.stream;


import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamMain {

    public static void main(String[] args) {

        //Stream Creation
        //Option 1
        String[] arr = new String[]{"a", "b", "c"};
        Stream<String> stream = Arrays.stream(arr);

        //Option 2
        stream = Stream.of("a", "b", "c");

        //Option 3
        List<Integer> list = Arrays.asList(20, 10, 15, 24, 55, 47, 16, 87, 88);
        Stream<Integer> stream1 = list.stream();

        //Multi-threading with Streams
        //Note that the o/p will be in different order due to parallelism
        list.parallelStream().forEach(element -> System.out.println(element));

        //Stream Operations

        //count & distinct
        long count = list.stream().distinct().count();
        System.out.println("Count = "+count);

        //Iterating - Normal
        boolean bool = false;
        for (Integer integer : list) {
            if (integer == 24) {
                bool = true;
            }
        }
        System.out.println("List contains 24 = "+bool);

        //Iterating - Stream
        boolean isExist = list.stream().anyMatch(element -> element == 24);
        System.out.println("List contains 24 = "+isExist);

        //Filtering
        ArrayList<String> list1 = new ArrayList<String>();
        list1.add("One");
        list1.add("OneAndOnly");
        list1.add("Derek");
        list1.add("Change");
        list1.add("factory");
        list1.add("justBefore");
        list1.add("Italy");
        list1.add("Italy");
        list1.add("Thursday");
        list1.add("");
        list1.add("");

        //code creates a Stream<String> of the List<String>, finds all elements
        // of this stream which contain char “d” and creates a new stream containing only the filtered elements
        Stream<String> stream2 = list1.stream().filter(element -> element.contains("d"));
        stream2.forEach(System.out::println);

        //Mapping
        List<String> uris = new ArrayList<>();
        uris.add("C:\\My.txt");
        //code above converts Stream<String> to the Stream<Path>
        // by applying a specific lambda expression to every element of the initial Stream
        Stream<Path> stream3 = uris.stream().map(uri -> Paths.get(uri));

        //Matching
        boolean isValid = list1.stream().anyMatch(element -> element.contains("h")); // true
        boolean isValidOne = list1.stream().allMatch(element -> element.contains("h")); // false
        boolean isValidTwo = list1.stream().noneMatch(element -> element.contains("h")); // false


        //Reduction
        //Imagine that you have a List<Integer> and you want to have a sum
        // of all these elements and some initial Integer (in this example 23).
        // So, you can run the following code and result will be 26 (23 + 1 + 1 + 1).
        List<Integer> integers = Arrays.asList(1, 1, 1);
        Integer reduced = integers.stream().reduce(23, (a, b) -> a + b);

        //Collecting
        List<String> resultList
                = list1.stream().map(element -> element.toUpperCase()).collect(Collectors.toList());
    }
}
