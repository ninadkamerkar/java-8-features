package com.ninad.java8.features.method_interface;


import java.util.Arrays;
import java.util.List;

public class MethodReferenceExample1 {
	
	public static void main(String[] args) {

		//Reference to a static method
		List<Integer> numbers = Arrays.asList(20, 10, 15, 24, 55, 47, 16, 87, 88);

		// Print even numbers using lambda expression
		numbers.stream().map((n) -> EvenOddCheck.isEven(n))
				.forEach((n) -> System.out.println(n));

		// Print even numbers using method references
		numbers.stream().map(EvenOddCheck::isEven)
				.forEach(System.out::println);
	}


}

class EvenOddCheck {
	public static boolean isEven(int n) {
		return n % 2 == 0;
	}
}